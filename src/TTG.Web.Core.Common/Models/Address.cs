﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
    public class Address
    {
        public int AddressId { get; set; }
        public string Line_1 { get; set; } = string.Empty;
        public string Line_2 { get; set; } = string.Empty;
        public int PostalCode { get; set; }
        [ForeignKey("AddressTypeId")]
        public int AddressTypeId { get; set; }
        [ForeignKey("TownId")]
        public int TownId { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
    }
}
