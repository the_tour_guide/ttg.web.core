﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
    public class Branch
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BranchId { get; set; }
        public string Name { get; set; } = string.Empty;
        public int Contact { get; set; }
        public string Email { get; set; } = string.Empty;
        [ForeignKey("BusinessId")]
        public int BusinessId { get; set; }
        [ForeignKey("BranchAddressId")]
        public int BranchAddressId { get; set; }
    }
}
