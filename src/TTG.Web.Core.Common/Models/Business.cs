﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
    public class Business
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BusinessId { get; set; } = 0;
        public string Name { get; set; } = string.Empty;
        public string? BusinessEmail { get; set; } = string.Empty;
        public int? BusinessCellPhone { get; set; } = 0;
        public string? BusinessDescription { get; set; } = string.Empty;
        public IndustryType IndustryTypes { get; set; }
        public Vendor Vendors { get; set; } 
        public BusinessType BusinessTypes { get; set; }
        public string? BusinessAddress { get; set; } = string.Empty;

        [ForeignKey("BusinessTypeId")]
        public int BusinessTypeId { get; set; } = 0;
        
        [ForeignKey("IndustryTypeId")]
        public int IndustryTypeId { get; set; } = 0;

        [ForeignKey("VendorId")]
        public int VendorId { get; set; } = 0;

    }
}
