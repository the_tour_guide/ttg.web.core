﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
    public class BusinessType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BusinessTypeId { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
    }
}
