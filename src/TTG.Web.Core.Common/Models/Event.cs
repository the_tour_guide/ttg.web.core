﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
	public class Event
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int EventId { get; set; }
		public string Name { get; set; } = string.Empty;
        public string Desc { get; set; } = string.Empty;
        public string Rules { get; set; } = string.Empty;
        [ForeignKey("BranchId")]
        public int BranchId { get; set; }
        public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
        public DateTime Updated { get; set; }
        public bool IsActive { get; set; } = false;
        public string Status { get; set; } = string.Empty;
        public bool IsDeleted { get; set; } = false;
        public bool IsLocked { get; set; } = false;
        public int? VenueId { get; set; }
        [ForeignKey("VenueId")]
        public Venue Venues { get; set; }
        [ForeignKey("VendorId")]
        public int VendorId { get; set; }
        public Vendor ChangedByVendor { get; set; }
    }
}

