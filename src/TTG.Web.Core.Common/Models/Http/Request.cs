﻿using System;
namespace TTG.Web.Core.Common.Models.Http
{
	public class Request<T> : Request
	{
		public T? Data { get; set; }
	}

	public class Request
	{
		public Guid SessionID { get; set; }
		public int UserID { get; set; }
	}
}

