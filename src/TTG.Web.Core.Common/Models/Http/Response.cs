﻿using System;
namespace TTG.Web.Core.Common.Models.Http
{
	public class Response<T> : Response
	{
		public T? Data { get; set; }
	}

	public class Response
	{
		public bool IsSuccesful { get; set; }
		public string ErrorMessage { get; set; } = string.Empty;
    }
}

