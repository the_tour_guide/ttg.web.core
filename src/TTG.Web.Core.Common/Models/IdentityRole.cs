﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTG.Web.Core.Common.Models
{
    public class IdentityRole
    {
        public const string ADMINISTRATOR = "d8ed3812-5783-4f5d-a4fe-1e46385f943d";
        public const string EDITOR = "db468840-00e9-483e-9c52-3763fbc206dc";
        public const string GUEST = "e8892710-7efe-48e2-8661-824379d0303c";
    }
}
