﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace TTG.Web.Core.Common.Models
{
    public class IdentityUser: Microsoft.AspNetCore.Identity.IdentityUser
    {
        public DateTime Created { get; set; }
        public DateTime LastLogin { get; set; }
        public bool IsHostAdmin { get; set; }
    }
}
