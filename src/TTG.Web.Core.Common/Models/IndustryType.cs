﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
    public class IndustryType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IndustryTypeId { get; set; }
        public string Name { get; set; } = string.Empty;
    }
}
