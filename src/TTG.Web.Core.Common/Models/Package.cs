﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
	public class Package
	{
		public int PackageId { get; set; }
		public string Name { get; set; }
		public int Total { get; set; }
		public double Price { get; set; }
        [ForeignKey("PackageTypeId")]
        public int PackageTypeId { get; set; }
        [ForeignKey("EventId")]
        public int EventId { get; set; }
		public DateTime Created { get; set; } = DateTime.Now;
		public DateTime Updated { get; set; }
		public bool IsDeleted { get; set; } = false;
		public bool IsLocked { get; set; } = false;
	}
}

