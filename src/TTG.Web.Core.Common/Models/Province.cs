﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
    public class Province
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProvinceId { get; set; }
        public string ProvinceCode { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        [ForeignKey("CountryId")]
        public int CountryId { get; set; }
    }
}
