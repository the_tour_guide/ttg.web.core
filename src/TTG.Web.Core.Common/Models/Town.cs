﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
    public class Town
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TownId { get; set; }
        public string TownCode { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        [ForeignKey("ProvinceId")]
        public int ProvinceId { get; set; }
    }
}

