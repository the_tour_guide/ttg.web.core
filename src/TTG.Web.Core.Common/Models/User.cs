﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string DisplayName { get; set; } = string.Empty;
        public DateTime DOB { get; set; }
        public string Email { get; set; } = string.Empty;
        public int ?UserType { get; set; }
        public string ?CurrentLocation { get; set; }
        public string ?Firstname { get; set; }
        public string ?Lastname { get; set; }
        public string Password { get; set; }
        public int ?Contact { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
        public DateTime Updated { get; set; }
        public DateTime LastLogin { get; set; }
        public bool IsSendMarketing { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public bool IsLocked { get; set; } = false;
        public bool IsBlocked { get; set; } = false;
        public bool IsActive { get; set; } = false;
    }
}

