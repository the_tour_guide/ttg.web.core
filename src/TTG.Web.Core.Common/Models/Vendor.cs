﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
    public class Vendor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VendorId { get; set; } = 0;
        public string? Firstname { get; set; } = string.Empty;
        public string? Lastname { get; set; } = string.Empty;
        public string? Username { get; set; } = string.Empty;
        public string? Email { get; set; } = string.Empty;
        public int CellPhone { get; set; } = 0;
        public string Location { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string ConfirmPassword { get; set; } = string.Empty;
        public string ?Description { get; set; } = string.Empty;
        public DateTime ?Created { get; set; } = DateTime.Now;
        public DateTime Updated { get; set; }
        public DateTime ?LastLogin { get; set; }
        public bool IsSendMarketing { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public bool IsLocked { get; set; } = false;
        public bool IsBlocked { get; set; } = false;
        public bool IsActive { get; set; } = false;
    }
}
