﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTG.Web.Core.Common.Models
{
	public class Venue
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int VenueId { get; set; }
        public string Name { get; set; }
		public string address { get; set; }
        [ForeignKey("VenueAddressId")]
        public int? VenueAddressId { get; set; }
		public Address Addresses { get; set; }
	}
}

