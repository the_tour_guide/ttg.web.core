﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TTG.Web.Core.Data.Migrations
{
    public partial class Data_BusinessTyesNew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
               table: "BusinessTypes",
               columns: new[] { "Name", "Label" },
               values: new object[,]
               {
                    { "company_pty_ltd", "Company (Pty) Ltd" },
                    { "close_corporation_cc", "Close Corporation CC" },
                    { "partnership", "Partnership" },
                    { "sole_proprietary", "Sole Proprietary" }
               });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
