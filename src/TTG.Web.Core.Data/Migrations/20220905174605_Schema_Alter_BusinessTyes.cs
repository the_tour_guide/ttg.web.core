﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TTG.Web.Core.Data.Migrations
{
    public partial class Schema_Alter_BusinessTyes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProductTypeId",
                table: "Businesses",
                newName: "BusinessCellPhone");

            migrationBuilder.AddColumn<int>(
                name: "CellPhone",
                table: "Vendors",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "Vendors",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "BusinessDescription",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<string>(
                name: "BusinessEmail",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BusinessTypeId",
                table: "Businesses",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_BusinessTypeId",
                table: "Businesses",
                column: "BusinessTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Businesses_IndustryTypeId",
                table: "Businesses",
                column: "IndustryTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_BusinessTypes_BusinessTypeId",
                table: "Businesses",
                column: "BusinessTypeId",
                principalTable: "BusinessTypes",
                principalColumn: "BusinessTypeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Businesses_IndustryTypes_IndustryTypeId",
                table: "Businesses",
                column: "IndustryTypeId",
                principalTable: "IndustryTypes",
                principalColumn: "IndustryTypeId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_BusinessTypes_BusinessTypeId",
                table: "Businesses");

            migrationBuilder.DropForeignKey(
                name: "FK_Businesses_IndustryTypes_IndustryTypeId",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_BusinessTypeId",
                table: "Businesses");

            migrationBuilder.DropIndex(
                name: "IX_Businesses_IndustryTypeId",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "CellPhone",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "BusinessEmail",
                table: "Businesses");

            migrationBuilder.DropColumn(
                name: "BusinessTypeId",
                table: "Businesses");

            migrationBuilder.RenameColumn(
                name: "BusinessCellPhone",
                table: "Businesses",
                newName: "ProductTypeId");

            migrationBuilder.AlterColumn<string>(
                name: "BusinessDescription",
                table: "Businesses",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }
    }
}
