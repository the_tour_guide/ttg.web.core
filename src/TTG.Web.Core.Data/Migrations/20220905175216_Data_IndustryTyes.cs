﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TTG.Web.Core.Data.Migrations
{
    public partial class Data_IndustryTyes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
               table: "IndustryTypes",
               columns: new[] { "Name"},
               values: new object[,]
               {
                    { "Entertainment"}
               });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
