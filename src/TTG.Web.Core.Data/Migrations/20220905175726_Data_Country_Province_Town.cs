﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TTG.Web.Core.Data.Migrations
{
    public partial class Data_Country_Province_Town : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
               table: "Countries",
               columns: new[] { "Name", "CountryCode" },
               values: new object[,]
               {
                    { "South Africa", "RSA" }
               });

            migrationBuilder.InsertData(
               table: "Provinces",
               columns: new[] { "Name", "ProvinceCode", "CountryId" },
               values: new object[,]
               {
                    { "Western Cape", "WC", 1 },
                    { "Gauteng", "GP", 1 },
                    { "KwaZulu-Natal", "KZN", 1 },
                    { "Eastern Cape", "EC", 1 }
               });

            migrationBuilder.InsertData(
               table: "Towns",
               columns: new[] { "Name", "TownCode", "ProvinceId" },
               values: new object[,]
               {
                    { "Cape Town", "CPT", 1 }
               });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
