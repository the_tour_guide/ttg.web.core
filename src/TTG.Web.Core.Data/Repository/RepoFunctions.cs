﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TTG.Web.Core.Common.Models;

namespace TTG.Web.Core.Data.Repository
{
    public class RepoFunctions<T> where T : class
    {
        protected readonly TTGDbContext _context;
        public RepoFunctions(TTGDbContext context)
        {
            _context = context;
        }

        public async Task<List<T>> GetAllAsync()
        {
            try
            {
                return await _context.Set<T>().ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when creating DB entity of type " + typeof(T), ex);
            }
        }

        public async Task<int> CreateAsync(T input)
        {
            try
            {
                _context.Add(input);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when creating DB entity of type " + typeof(T) + " with input of " + input, ex);
            }
        }

        public async Task<int> UpdateAsync(T input)
        {
            try
            {
                _context.Update(input);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when update DB entity of type " + typeof(T) + " with input of " + input, ex);
            }
        }

        public async Task<int> DeleteAsync(T input)
        {
            try
            {
                _context.Remove(input);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when remove DB entity of type " + typeof(T) + " with input of " + input, ex);
            }
        }
    }
}

