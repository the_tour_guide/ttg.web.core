﻿using System;
using Microsoft.EntityFrameworkCore;
using TTG.Web.Core.Common.Models;

namespace TTG.Web.Core.Data.Repository
{
	public class TTGDbContext : DbContext
	{
		public DbSet<Event> Events { get; set; }
		public DbSet<Venue> Venues { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Town> Towns { get; set; }
        public DbSet<AddressType> AddressTypes { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<PackageType> PackageTypes { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<IndustryType> IndustryTypes { get; set; }
        public DbSet<Business> Businesses { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<MediaFile> MediaFiles { get; set; }
        public DbSet<BusinessType> BusinessTypes { get; set; }
        public DbSet<Vendor> Vendors { get; set; }

        public TTGDbContext(DbContextOptions<TTGDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
            base.OnModelCreating(modelBuilder);
            //base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Event>()
            //    .Property(e => e.EventId)
            //    .ValueGeneratedOnAdd();

            //modelBuilder.Entity<Event>()
            //    .Property(e => e.StartDate)
            //    .IsRequired();

            //modelBuilder.Entity<Event>()
            //    .Property(e => e.EndDate)
            //    .IsRequired();

            //modelBuilder.Entity<Event>()
            //    .Property(e => e.Rules)
            //    .IsRequired();

            //modelBuilder.Entity<Event>()
            //    .Property(e => e.Packages)
            //    .IsRequired();

            //modelBuilder.Entity<Venue>()
            //    .Property(e => e.Id)
            //    .ValueGeneratedOnAdd();

            //modelBuilder.Entity<Venue>()
            //    .Property(e => e.Category)
            //    .IsRequired();
        }
	}
}

