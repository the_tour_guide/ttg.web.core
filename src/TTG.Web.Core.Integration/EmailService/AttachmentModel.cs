﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTG.Web.Core.Integration.EmailService
{
    public class AttachmentModel
    {
        public const string ATTACHMENT = "attachment";
        public const string INLINE = "inline";

        public string Type { get; set; }
        public string Filename { get; set; }
        public string Disposition { get; set; }
        public string ContentId { get; set; }
        public string Content { get; set; }
    }
}
