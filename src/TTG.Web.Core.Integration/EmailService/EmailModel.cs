﻿//using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Text;
namespace TTG.Web.Core.Integration.EmailService
{
    public class EmailModel
    {
        public EmailModel()
        {
            //Attachments = new List<Attachment>();
        }

        public string From { get; set; }
        
        public string To { get; set; }
        
        public string ReplyTo { get; set; }

        public string Subject { get; set; }
        
        public string HtmlContents { get; set; }
        
        public string PlainTextContents { get; set; }

        //public List<Attachment> Attachments { get; set; }

        public Dictionary<string, string> Headers { get; set; }


    }
}
