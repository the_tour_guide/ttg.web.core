﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTG.Web.Core.Integration.EmailService
{
    public class EmailOptions
    {
        public string FromAddress { get; set; }
    }
}
