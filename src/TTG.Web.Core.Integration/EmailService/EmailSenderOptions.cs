﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTG.Web.Core.Integration.EmailService
{
    public class EmailSenderOptions
    {
        public string User { get; set; }
        public string Key { get; set; }
    }
}
