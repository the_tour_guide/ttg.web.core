﻿//using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Options;
//using SendGrid;
//using SendGrid.Helpers.Mail;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Text;
//using System.Threading.Tasks;
//using CameraTrap.Email.Interfaces;

//namespace TTG.Web.Core.Integration.EmailService
//{
//    public class EmailSenderSendGrid : IEmailSenderService
//    {
//        private readonly ILogger _logger;

//        public EmailSenderOptions Options { get; } //set only via Secret Manager
        
//        public EmailSenderSendGrid(IOptions<EmailSenderOptions> optionsAccessor, ILogger<EmailSenderSendGrid> logger)
//        {
//            Options = optionsAccessor.Value;
//            this._logger = logger;
//        }

//        public async Task SendEmailAsync(EmailModel emailModel)
//        {
//            var client = new SendGridClient(Options.Key);

//            var msg = MailHelper.CreateSingleEmail(new EmailAddress(emailModel.From), new EmailAddress(emailModel.To), emailModel.Subject, emailModel.PlainTextContents, emailModel.HtmlContents);

//            if (emailModel.Attachments.Any())
//            {
//                msg.Attachments = emailModel.Attachments
//                    .Select(a => new Attachment
//                    {
//                        Content = a.Content,
//                        ContentId = a.ContentId,
//                        Disposition = a.Disposition,
//                        Filename = a.Filename,
//                        Type = a.Type
//                    }).ToList();
//            }

//            if (emailModel.Headers != null)
//            {
//                msg.Headers = emailModel.Headers;
//            }

//            if (emailModel.ReplyTo != null)
//            {
//                msg.ReplyTo = new EmailAddress(emailModel.ReplyTo);
//            }

//            var response = await client.SendEmailAsync(msg);

//            if (response.StatusCode != HttpStatusCode.Accepted)
//            {
//                var responseBody = await response.Body.ReadAsStringAsync();

//                _logger.LogError($"Bad request while sending email via SendGrid: {response.StatusCode} - {responseBody}");

//                return;
//            }

//            //Do we want to log these messages send and later query status.
//            var messageId = response.Headers
//                .FirstOrDefault(a => a.Key == "X-Message-Id")
//                .Value.First();


//            _logger.LogInformation($"Email sent via SendGrid with message id {messageId}");
//        }

//    }
//}
