﻿//using Microsoft.AspNetCore.Hosting;
//using Microsoft.AspNetCore.StaticFiles;
//using Microsoft.Extensions.Logging;
//using SendGrid.Helpers.Mail;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Threading.Tasks;
//using Email.Interfaces;
//using CameraTrap.Utility.RazorViewRenderer;
//using Microsoft.Extensions.Options;


//namespace TTG.Web.Core.Integration.EmailService
//{
//    public class EmailService : IEmailService
//    {
//        private readonly IEmailSenderService _sendGridEmailService;
//        private readonly IViewRenderer _renderer;
//        private readonly ILogger<EmailService> _logger;
//        private readonly EmailOptions _emailOptions;

//        public EmailService(IEmailSenderService sendGridEmailService, IViewRenderer renderer, IOptions<EmailOptions> emailOptions, ILogger<EmailService> logger)
//        {
//            _sendGridEmailService = sendGridEmailService;
//            _emailOptions = emailOptions.Value;
//            _renderer = renderer;
//            _logger = logger;
//        }

//        public async Task SendEmailAsync<TModel>(string templateName, string toAddress, string fromAddress, TModel emailModel,  List<AttachmentModel> attachmentModels)
//        {
//            fromAddress = fromAddress == null?_emailOptions.FromAddress:fromAddress;

//            List<Attachment> attachments = new List<Attachment>();

//            //Embedded attachments from in a template
//            //Get the base path of our email assembly
//            var assemblyUri = System.Reflection.Assembly.GetExecutingAssembly().Location;

//            _logger.LogDebug($"The assembly uri is: {assemblyUri}");

//            var assemblyPath = new Uri(assemblyUri).LocalPath;

//            _logger.LogDebug($"The assembly path is: {assemblyPath}");

//            var rootDir = System.IO.Path.GetDirectoryName(assemblyPath);

//            _logger.LogDebug($"Root directory for template resolution is: {rootDir}");

//            var embeddedSharedPath = Path.Combine(rootDir, "EmailTemplates", "Shared", "images");

//            _logger.LogDebug($"Shared directory for embedded images is: {embeddedSharedPath}");

//            //Attach embedded shared images
//            var embeddedShared = this.GetImages(embeddedSharedPath);

//            foreach (var m in embeddedShared)
//            {
//                attachments.Add(new Attachment()
//                {
//                    Content = m.Content,
//                    ContentId = m.ContentId,
//                    Disposition = m.Disposition,
//                    Filename = m.Filename,
//                    Type = m.Type
//                });
//            }

//            var embeddedTemplatePath = Path.Combine(rootDir, $"EmailTemplates", $"{templateName}", "images");

//            _logger.LogDebug($"Template directory for embedded images is: {embeddedTemplatePath}");

//            var embeddedTemplate = this.GetImages(embeddedTemplatePath);
            

//            foreach (var m in embeddedTemplate)
//            {
//                attachments.Add(new Attachment()
//                {
//                    Content = m.Content,
//                    ContentId = m.ContentId,
//                    Disposition = m.Disposition,
//                    Filename = m.Filename,
//                    Type = m.Type
//                });
//            }

//            //Generic attachments
//            foreach (var m in attachmentModels)
//            {
//                attachments.Add(new Attachment()
//                {
//                    Content = m.Content,
//                    Type = m.Type,
//                    Filename = m.Filename,
//                    Disposition = m.Disposition,
//                    ContentId = m.ContentId,
//                });
//            }

//            var options = new Dictionary<string, object>()
//            {
//                {"FromAddress", _emailOptions.FromAddress}
//            };

//            var bodyHtml = await _renderer.RenderViewToStringAsync($"EmailTemplates/{templateName}/body.html.cshtml", emailModel, options);
//            var bodyText = await _renderer.RenderViewToStringAsync($"EmailTemplates/{templateName}/body.txt.cshtml", emailModel, options);
//            var subject = await _renderer.RenderViewToStringAsync($"EmailTemplates/{templateName}/subject.cshtml", emailModel, options);

//            await _sendGridEmailService.SendEmailAsync(new EmailModel()
//            {
//                From = fromAddress,
//                Subject = subject,
//                PlainTextContents = bodyText,
//                HtmlContents = bodyHtml,
//                To = toAddress,
//                Attachments = attachments
//            });
//        }

//        private IEnumerable<AttachmentModel> GetImages(string path) {

//            var result = new List<AttachmentModel>();

//            if (Directory.Exists(path) == false) return result;

//            var provider = new FileExtensionContentTypeProvider();

//            foreach (var filePath in Directory.GetFiles(path))
//            {
//                var id = Path.GetFileName(filePath);
//                var fileName = Path.GetFileName(filePath);
                
//                var mimeType = (string)null;
//                if (provider.TryGetContentType(filePath, out mimeType) == false)
//                {
//                    mimeType = "application/octet-stream";
//                }

//                var content = Convert.ToBase64String(System.IO.File.ReadAllBytes(filePath));
//                result.Add(
//                new AttachmentModel()
//                {
//                    ContentId = id,
//                    Disposition = "inline",
//                    Filename = fileName,
//                    Type = mimeType,
//                    Content = content

//                });
//            }

//            return result;
            
//        }
//    }
//}
