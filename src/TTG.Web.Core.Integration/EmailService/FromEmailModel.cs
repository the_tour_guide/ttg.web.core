﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTG.Web.Core.Integration.EmailService
{
    public class FromEmailModel
    {
        public string Address { get; set; }
        public string Name { get; set; }
    }
}
